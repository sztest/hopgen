-- LUALOCALS < ---------------------------------------------------------
local math, minetest
    = math, minetest
local math_ceil
    = math.ceil
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local hgapi = _G[modname]

local schemkeys = {
	["."] = {name = "air", prob = 0},
	["#"] = {name = "world:black"},
	["*"] = {name = "air"},
	r = {name = "buildings:red"},
	g = {name = "buildings:green"},
	b = {name = "buildings:blue"},
	y = {name = "buildings:yellow"},
	p = {name = "buildings:purple"},
	k = {name = "buildings:black"},

}

local altars = {}

local clear9 = {
	"*********",
	"*********",
	"*********",
	"*********",
	"*********",
	"*********",
	"*********",
	"*********",
	"*********",
}
local altar1, size1 = hgapi.ezschematic(schemkeys,
	{
		{
			"yyyyyyyyy",
			"ygggggggy",
			"ygkrkrkgy",
			"ygryyyrgy",
			"ygkyrykgy",
			"ygryyyrgy",
			"ygkrkrkgy",
			"ygggggggy",
			"yyyyyyyyy",
		},
		{
			"*********",
			"*********",
			"***r*r***",
			"**r***r**",
			"****r****",
			"**r***r**",
			"***r*r***",
			"*********",
			"*********",
		},
		{
			"*********",
			"*********",
			"***g*g***",
			"**g***g**",
			"****r****",
			"**g***g**",
			"***g*g***",
			"*********",
			"*********",
		},
		{
			"*********",
			"*********",
			"*********",
			"*********",
			"****r****",
			"*********",
			"*********",
			"*********",
			"*********",
		},
		{
			"*********",
			"*********",
			"*********",
			"*********",
			"****g****",
			"*********",
			"*********",
			"*********",
			"*********",
		},
		clear9, clear9, clear9, clear9,
		clear9, clear9, clear9, clear9,
		clear9, clear9, clear9, clear9,
	})
altars[#altars + 1] = {schem = altar1, size = size1}

local altar2, size2 = hgapi.ezschematic(schemkeys,
	{
		{
			"yyyyyyyyy",
			"ykkrkrkky",
			"ykrkrkrky",
			"yrkryrkry",
			"ykryryrky",
			"yrkryrkry",
			"ykrkrkrky",
			"ykkrkrkky",
			"yyyyyyyyy",
		},
		{
			"*********",
			"*b**b**b*",
			"*********",
			"*********",
			"*b**b**b*",
			"*********",
			"*********",
			"*b**b**b*",
			"*********",
		},
		{
			"*********",
			"*g**g**g*",
			"*********",
			"*********",
			"*g**b**g*",
			"*********",
			"*********",
			"*g**g**g*",
			"*********",
		},
		{
			"*********",
			"*********",
			"*********",
			"*********",
			"****b****",
			"*********",
			"*********",
			"*********",
			"*********",
		},
		{
			"*********",
			"*********",
			"*********",
			"*********",
			"****g****",
			"*********",
			"*********",
			"*********",
			"*********",
		},
		clear9, clear9, clear9, clear9,
		clear9, clear9, clear9, clear9,
		clear9, clear9, clear9, clear9,
	})
altars[#altars + 1] = {schem = altar2, size = size2}

local clear15 = {
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
	"***************",
}

local altar3, size3 = hgapi.ezschematic(schemkeys,
	{
		{
			"yyyyyyyyyyyyyyy",
			"ykkkkkkykkkkkky",
			"ykbbbbbybbbbbky",
			"ykbkkkkykkkkbky",
			"ykbkbykykybkbky",
			"ykbkykkykkykbky",
			"ykbkkkkykkkkbky",
			"yyyyyyyryyyyyyy",
			"ykbkkkkykkkkbky",
			"ykbkykkykkykbky",
			"ykbkbykykybkbky",
			"ykbkkkkykkkkbky",
			"ykbbbbbybbbbbky",
			"ykkkkkkykkkkkky",
			"yyyyyyyyyyyyyyy",
		},
		{
			"***************",
			"*kk*kk***kk*kk*",
			"*k***********k*",
			"***kkk***kkk***",
			"*k*k*******k*k*",
			"*k*k*k***k*k*k*",
			"***************",
			"*******b*******",
			"***************",
			"*k*k*k***k*k*k*",
			"*k*k*******k*k*",
			"***kkk***kkk***",
			"*k***********k*",
			"*kk*kk***kk*kk*",
			"***************",
		},
		{
			"***************",
			"*kk*kk***kk*kk*",
			"*k***********k*",
			"***kkk***kkk***",
			"*k*k*******k*k*",
			"*k*k*k***k*k*k*",
			"***************",
			"*******b*******",
			"***************",
			"*k*k*k***k*k*k*",
			"*k*k*******k*k*",
			"***kkk***kkk***",
			"*k***********k*",
			"*kk*kk***kk*kk*",
			"***************",
		},
		{
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"*****k***k*****",
			"***************",
			"*******p*******",
			"***************",
			"*****k***k*****",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
		},
		{
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"*****p***p*****",
			"***************",
			"*******p*******",
			"***************",
			"*****p***p*****",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
		},
		{
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"*******g*******",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
			"***************",
		},
		clear15, clear15, clear15, clear15,
		clear15, clear15, clear15, clear15,
		clear15, clear15, clear15, clear15,
	})
altars[#altars + 1] = {schem = altar3, size = size3}

local cityheight = hgapi.cityheight
hgapi.register_mapgen_shared({
		label = "generate altars",
		schematics = true,
		priority = -200,
		func = function(minp, maxp, _, _, vm, _, _, rng)
			if minp.y > 18 then return end
			if maxp.y < 8 then return end
			for _ = 1, (maxp.x - minp.x) * (maxp.z - minp.z) / 200 do
				local x = rng(minp.x, maxp.x - 2)
				local z = rng(minp.z, maxp.z - 2)
				x = math_ceil(x / 2) * 2 + 1
				z = math_ceil(z / 2) * 2 + 1
				if cityheight({x = x, y = z}) > rng(4, 10) then
					local picked = altars[rng(1, #altars)]
					if picked.size.x % 4 == 1 then x = x + 1 end
					if picked.size.z % 4 == 1 then z = z + 1 end
					if maxp.x - x >= picked.size.x
					and maxp.z - z >= picked.size.z then
						minetest.place_schematic_on_vmanip(vm,
							{x = x, y = 8, z = z}, picked.schem,
							nil, nil, true)
						return
					end
				end
			end
		end
	})
