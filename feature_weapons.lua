-- LUALOCALS < ---------------------------------------------------------
local math, minetest, pairs
    = math, minetest, pairs
local math_ceil
    = math.ceil
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local hgapi = _G[modname]

local schemkeys = {
	["."] = {name = "air", prob = 0},
	["#"] = {name = "world:black"},
	["*"] = {name = "air"},
	r = {name = "world:red"},
	g = {name = "world:green"},
	b = {name = "world:blue"},
	y = {name = "world:yellow"},
	p = {name = "world:purple"},
}

local base = {".g.", "grg", ".g."}
local red = {".*.", "*r*", ".*."}
local blue = {".*.", "*b*", ".*."}
local top = {".*.", "*y*", ".*."}
local clear = {"...", ".*.", "..."}

local fireworks = {{}, {red}, {blue}}
for k, v in pairs(fireworks) do
	local slices = {base}
	for i = 1, #v do slices[#slices + 1] = v[i] end
	slices[#slices + 1] = top
	for _ = 1, 10 do slices[#slices + 1] = clear end
	local schem, size = hgapi.ezschematic(schemkeys, slices)
	fireworks[k] = {schem = schem, size = size}
end

local ionbase = {
	"....###....",
	".####g####.",
	".#yyybyyy#.",
	".#ybbbbby#.",
	"##yb#b#by##",
	"#gbbbrbbbg#",
	"##yb#b#by##",
	".#ybbbbby#.",
	".#yyybyyy#.",
	".####g####.",
	"....###....",
}
local iontower = {
	"....***....",
	".*********.",
	".*********.",
	".*********.",
	"***********",
	"*****p*****",
	"***********",
	".*********.",
	".*********.",
	".*********.",
	"....***....",
}
local ionclear = {
	"....***....",
	".*********.",
	".*********.",
	".*********.",
	"***********",
	"***********",
	"***********",
	".*********.",
	".*********.",
	".*********.",
	"....***....",
}
local ionschem, ionsize = hgapi.ezschematic(schemkeys, {
		ionbase,
		iontower,
		iontower,
		iontower,
		ionclear, ionclear, ionclear, ionclear,
		ionclear, ionclear, ionclear, ionclear,
		ionclear, ionclear, ionclear, ionclear,
	})
fireworks[4] = {schem = ionschem, size = ionsize}

local weapons = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	2, 2, 2, 2, 2, 2, 2,
	3, 3, 3,
	4,
}
for i = 1, #weapons do weapons[i] = fireworks[weapons[i]] end

local cityheight = hgapi.cityheight
hgapi.register_mapgen_shared({
		label = "generate weapons",
		schematics = true,
		priority = -100,
		func = function(minp, maxp, _, _, vm, _, _, rng)
			if minp.y > 18 then return end
			if maxp.y < 8 then return end
			for _ = 1, (maxp.x - minp.x) * (maxp.z - minp.z) / 100 do
				local x = rng(minp.x, maxp.x - 2)
				local z = rng(minp.z, maxp.z - 2)
				x = math_ceil(x / 2) * 2 + 1
				z = math_ceil(z / 2) * 2 + 1
				if cityheight({x = x, y = z}) > rng(1, 10) then
					local picked = weapons[rng(1, #weapons)]
					if maxp.x - x >= picked.size.x
					and maxp.z - z >= picked.size.z then
						minetest.place_schematic_on_vmanip(vm,
							{x = x, y = 8, z = z}, picked.schem,
							nil, nil, true)
					end
				end
			end
		end
	})
