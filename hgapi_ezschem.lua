-- LUALOCALS < ---------------------------------------------------------
local error, ipairs, minetest
    = error, ipairs, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local hgapi = _G[modname]

function hgapi.ezschematic(key, yslices, init)
	local totals = {}
	local size = {}
	local data = {}
	size.y = #yslices
	for y, ys in ipairs(yslices) do
		if size.z and size.z ~= #ys then error("inconsistent z size") end
		size.z = #ys
		for z, zs in ipairs(ys) do
			if size.x and size.x ~= #zs then error("inconsistent x size") end
			size.x = #zs
			for x = 1, zs:len() do
				local node = key[zs:sub(x, x)]
				if node and node.name then
					totals[node.name] = (totals[node.name] or 0) + 1
				end
				data[(z - 1) * size.x * size.y + (y - 1) * size.x + x] = node
			end
		end
	end
	init = init or {}
	init.size = size
	init.data = data

	return minetest.register_schematic(init), size
end
