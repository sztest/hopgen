-- LUALOCALS < ---------------------------------------------------------
local math, minetest
    = math, minetest
local math_ceil
    = math.ceil
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local hgapi = _G[modname]

local cityheight = hgapi.cityheight

local cid = minetest.get_content_id("world:black")
hgapi.register_mapgen_shared({
		label = "generate cityscape",
		func = function(minp, maxp, area, data, _, _, _, rng)
			if minp.y > 18 then return end
			if maxp.y < 8 then return end
			local minz = math_ceil(minp.z / 2) * 2
			local minx = math_ceil(minp.x / 2) * 2
			for z = minz, maxp.z, 2 do
				local pos = {y = z}
				for x = minx, maxp.x, 2 do
					pos.x = x
					local d = cityheight(pos)
					if d > 0 then
						for y = 8, rng(8, 8 + d) do
							data[area:index(x, y, z)] = cid
						end
					end
				end
			end
		end
	})
